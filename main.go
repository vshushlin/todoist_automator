package main

import (
	"gitlab.com/vshushlin/todoist_automator/internal/gitlab"
	"gitlab.com/vshushlin/todoist_automator/internal/sync"
	"gitlab.com/vshushlin/todoist_automator/internal/todoist"
	"os"
)

func main() {
	todoistToken := os.Getenv("TODOIST_TOKEN")
	todoistClient, err := todoist.NewClient(todoistToken, "todos")
	if err != nil {
		panic(err)
	}

	gitlabToken := os.Getenv("GL_TOKEN")
	gitlabClient, err := gitlab.NewClient(gitlabToken)
	if err != nil {
		panic(err)
	}

	syncManager := sync.New(gitlabClient, todoistClient)
	err = syncManager.Sync()
	if err != nil {
		panic(err)
	}

	return
}
