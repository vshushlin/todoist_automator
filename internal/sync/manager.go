package sync

import (
	"fmt"
	"gitlab.com/vshushlin/todoist_automator/internal/gitlab"
	"gitlab.com/vshushlin/todoist_automator/internal/todoist"
)

type GitlabClient interface {
	GetOpenTodos() ([]gitlab.Todo, error)
}

type TodoistClient interface {
	GetOpenTasks() ([]todoist.Task, error)
	CreateTask(title, url, description string, labels []string, priority int) error
}

type Manager struct {
	gitlab  GitlabClient
	todoist TodoistClient
}

func New(gitlab GitlabClient, todoist TodoistClient) *Manager {
	return &Manager{
		gitlab:  gitlab,
		todoist: todoist,
	}
}

func (m *Manager) Sync() error {
	gitlabTodos, todoistOpenTasks, err := m.collectTaskLists()
	if err != nil {
		return err
	}

	fmt.Println("Got GitLab todos")
	for urlid, gitlabTodo := range gitlabTodos {
		fmt.Println(urlid, gitlabTodo.Title(), gitlabTodo.Description())
	}

	fmt.Println("\nGot Todoist open tasks")
	for urlid, todoistTask := range todoistOpenTasks {
		fmt.Println(urlid, todoistTask.Title())
	}

	fmt.Println("\nClosing todoist tasks closed in GitLab")
	for urlid, todoistTask := range todoistOpenTasks {
		if _, ok := gitlabTodos[urlid]; !ok {
			fmt.Println(urlid, todoistTask.Title())

			err := todoistTask.Close()
			if err != nil {
				return err
			}

			delete(todoistOpenTasks, urlid)
		}
	}

	fmt.Println("\nCreating todoist tasks created in GitLab")
	for urlid, gitlabTodo := range gitlabTodos {
		if _, ok := todoistOpenTasks[urlid]; !ok {
			fmt.Println(urlid, gitlabTodo.Title(), gitlabTodo.Description())

			err = m.todoist.CreateTask(gitlabTodo.Title(), gitlabTodo.URLId(),
				gitlabTodo.Description(), gitlabTodo.Labels(), gitlabTodo.Priority())
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (m *Manager) collectTaskLists() (map[string]gitlab.Todo, map[string]todoist.Task, error) {
	gitlabTodos, err := m.gitlab.GetOpenTodos()
	if err != nil {
		return nil, nil, err
	}

	todoistTasks, err := m.todoist.GetOpenTasks()
	if err != nil {
		return nil, nil, err
	}

	var gitlabTodosByURLId = make(map[string]gitlab.Todo)
	for _, todo := range gitlabTodos {
		gitlabTodosByURLId[todo.URLId()] = todo
	}

	var todoistOpenTasksByURLId = make(map[string]todoist.Task)
	for _, task := range todoistTasks {
		todoistOpenTasksByURLId[task.URLId()] = task
	}

	return gitlabTodosByURLId, todoistOpenTasksByURLId, nil
}
