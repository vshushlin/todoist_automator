package todoist

import (
	"context"
	"fmt"
	todoistApi "github.com/temoon/todoist-api"
	"regexp"
	"strings"
)

type Client struct {
	client         *todoistApi.Todoist
	todosProject   string
	todosProjectId string
}

type Task interface {
	URLId() string
	Title() string
	Close() error
}

type task struct {
	urlid       string // GitLab URL which is used as id for the sync purposes
	todoistTask *todoistApi.Task
	client      *todoistApi.Todoist
}

const (
	automatorLabelName = "todoist-automator"
)

var descriptionRegexp = regexp.MustCompile(`^\[(.*?)\]\((.*?)\)$`)
var markdownEscapeRegexp = regexp.MustCompile(`[\[\]\(\)]`)

func NewClient(token string, todosProject string) (*Client, error) {
	todoistOpts := &todoistApi.Opts{
		Token: token,
	}
	todoistClient := todoistApi.New(todoistOpts)

	todoProjectId, err := getGitLabTodoProjectId(todoistClient, todosProject)
	if err != nil {
		return nil, err
	}

	return &Client{
		client:         todoistClient,
		todosProject:   todosProject,
		todosProjectId: todoProjectId,
	}, nil
}

func getGitLabTodoProjectId(todoistClient *todoistApi.Todoist, todosProjectName string) (string, error) {
	todoProjectId := ""

	todoistProjects, err := todoistClient.GetProjects(context.TODO())
	if err != nil {
		return "", err
	}

	for _, project := range todoistProjects {
		if project.Name == todosProjectName {
			if todoProjectId != "" {
				return "", fmt.Errorf("Todoist Account has more that one project named \"%s\"", todosProjectName)
			}

			todoProjectId = project.Id
		}
	}
	return todoProjectId, nil
}

func (c *Client) GetOpenTasks() ([]Task, error) {
	tasks := []Task{}

	todoistTasks, err := c.client.GetTasks(context.TODO(), todoistApi.MakeGetTasksParams().
		WithFilter("##"+c.todosProject+" & @"+automatorLabelName))
	if err != nil {
		panic(err)
	}

	for _, todoistTask := range todoistTasks {
		urlId, err := extractURLIdFromDescription(todoistTask.Content)
		if err != nil {
			return nil, fmt.Errorf("%w while parsing %v", err, todoistTask)
		}

		tasks = append(tasks, &task{
			urlid:       urlId,
			todoistTask: &todoistTask,
			client:      c.client,
		})
	}

	return tasks, nil
}

func (c *Client) CreateTask(title, url, description string, labels []string, priority int) error {
	title = escapeMarkdown(title)
	description = escapeMarkdown(description)
	if len(title) > 400 {
		title = title[:400] // todoist has a limit of 500 characters
	}
	title = escapeMarkdown(title)

	labels = append(labels, automatorLabelName)
	content := fmt.Sprintf("[%s](%s)", title, url)

	params := todoistApi.MakeAddTaskParams().WithProjectId(c.todosProjectId).
		WithContent(content).WithDescription(description).WithLabels(labels).
		WithPriority(priority)
	_, err := c.client.AddTask(context.TODO(), params)
	return err
}

func extractURLIdFromDescription(description string) (string, error) {
	matches := descriptionRegexp.FindStringSubmatch(description)

	if len(matches) != 3 {
		return "", fmt.Errorf("Description \"%s\" doesn't match the format of [text](URLId)", description)
	}

	return matches[2], nil
}

func (t *task) URLId() string {
	return t.urlid
}

func (t *task) Title() string {
	return t.todoistTask.Content
}

func (t *task) Close() error {
	return t.client.CloseTask(context.TODO(), t.todoistTask.Id)
}

func escapeMarkdown(text string) string {
	// replace all new lines with spaces
	text = strings.ReplaceAll(text, "\n", " ")

	text = markdownEscapeRegexp.ReplaceAllStringFunc(text, func(match string) string {
		return `\` + match
	})

	// todoist will autoexpand links changing the markdown
	// remove any protocols to prevent this
	text = strings.ReplaceAll(text, "http://", "")
	text = strings.ReplaceAll(text, "https://", "")

	return text
}
