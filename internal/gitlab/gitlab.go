package gitlab

import (
	gitlabClient "github.com/xanzy/go-gitlab"
)

type Todo interface {
	URLId() string
	Title() string
	Description() string
	Labels() []string
	Priority() int
}

type Client struct {
	client *gitlabClient.Client
}

type todo struct {
	todo   *gitlabClient.Todo
	client *gitlabClient.Client
}

func NewClient(token string) (*Client, error) {
	client, err := gitlabClient.NewClient(token)
	if err != nil {
		return nil, err
	}

	return &Client{
		client: client,
	}, nil
}

func (m *Client) GetOpenTodos() ([]Todo, error) {
	gitLabTodos := []Todo{}
	page := 1
	for page > 0 {
		r, response, err := m.client.Todos.ListTodos(&gitlabClient.ListTodosOptions{
			ListOptions: gitlabClient.ListOptions{
				Page:    page,
				PerPage: 200,
			},
		})
		if err != nil {
			return nil, err
		}
		for _, t := range r {
			gitLabTodos = append(gitLabTodos, &todo{todo: t, client: m.client})
		}

		page = response.NextPage
	}
	return gitLabTodos, nil
}

func (t *todo) URLId() string {
	return t.todo.TargetURL
}

func (t *todo) Title() string {
	switch t.todo.ActionName {
	case gitlabClient.TodoAssigned:
		return "**" + t.todo.Author.Name + "** assigned " + t.todo.Target.Title
	case gitlabClient.TodoMentioned:
		return "**" + t.todo.Author.Name + "**: " + t.todo.Body
	case gitlabClient.TodoBuildFailed:
		return "The pipeline failed."
	case gitlabClient.TodoMarked:
		return t.todo.Body
	case gitlabClient.TodoApprovalRequired:
		return "Approval required."
	case gitlabClient.TodoDirectlyAddressed:
		return t.todo.Body
	case "review_requested":
		return t.todo.Target.Title
	case "member_access_requested":
		return t.todo.Target.Title
	case "review_submitted":
		return t.todo.Target.Title
	case "unmergeable":
		return t.todo.Target.Title
	case "merge_train_removed":
		return t.todo.Target.Title
	default:
		return "Unhandled type"
	}
}

func (t *todo) Description() string {
	switch t.todo.ActionName {
	default:
		return t.todo.Target.Title
	}
}

func (t *todo) Labels() []string {
	labels := []string{}

	switch t.todo.ActionName {
	case gitlabClient.TodoMentioned:
		labels = append(labels, "ping")
	case gitlabClient.TodoBuildFailed:
		labels = append(labels, "Pipeline failed")
	}

	return labels
}

func (t *todo) Priority() int {
	switch t.todo.ActionName {
	case gitlabClient.TodoMentioned:
		return 3
	case gitlabClient.TodoBuildFailed:
		return 3
	default:
		return 2
	}
}
